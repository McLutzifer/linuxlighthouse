# LinuxLighthouse

*A software that will take you on a journey to find the perfect Linux distribution.*  
*A lighthouse symbolizes guidance and support, which is exactly what this software aims to provide.*

## Background
Deciding on the right distribution can be challenging, as there are many different options, each with its own advantages and disadvantages. However, current solutions, such as the distrochooser.de website, are somewhat outdated and (in my opinion) incomplete, and lack important aspects and details. This gap is to be closed with this new software.

## Acknowledgment
This project is greatly inspired by the awesome website [distrochooser.de](https://github.com/distrochooser/distrochooser). This homage is meant stand on the shoulders of giants and start where distrochooser leaves off.

## License
This open source project is licensed under the GNU AGPLv3

## Project status
As of summer 2023, this project is still actively maintained.

## Contributing

This project utilizes [Conventional Commit Messages](https://www.conventionalcommits.org/en/v1.0.0/). Further information can be found in CONTRIBUTING.md   


## Further Roadmap
- [ ] make a web Version of the software
- [ ] extend the questionnaire beyond Linux distributions (possibly to terminals, desktop environments,  browsers, ...)  
- [ ] better handling of password credentials
- [ ] pack GUI in Docker Container
- [ ] extend test to include BSD?
- [ ] add History (previous test results) 
- [ ] upgrade HTTP to HTTPS 

## Description
A detailed documentation can be found in the directory "documentation"

## Visuals
*... TBA*

## Installation
*... TBA*

## Usage
*... TBA*
