module LinuxLighthouse.API {
    exports at.linuxlighthouse.api;
    requires java.net.http;
    requires jdk.httpserver;
    requires LinuxLighthouse.Classes;
    requires LinuxLighthouse.Backend;
    requires java.sql;

}