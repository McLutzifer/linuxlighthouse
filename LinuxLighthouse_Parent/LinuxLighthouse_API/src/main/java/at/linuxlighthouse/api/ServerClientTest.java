/**
 * A test client for the LinuxLighthouse server.
 * This class is used to test the server's endpoints by sending HTTP requests and
 * validating the responses.
 */


package at.linuxlighthouse.api;


import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class ServerClientTest {

    /**
     * The main method to execute the server client test.
     * It sends a GET request to the specified server endpoint and prints the response.
     *
     * @param args The command-line arguments (not used in this program).
     */

    public static void main(String[] args) {

        String serverUrl = "http://localhost:4712/test";


        // Create an HttpClient instance
        HttpClient client = HttpClient.newHttpClient();

        // Create a GET request to the server URL
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(serverUrl)).build();


        try {
            // Send the request and obtain the response
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

            // Get the response status code and body
            int statusCode = response.statusCode();
            String responseBody = response.body();


            // Print the response
            System.out.println("Response status code: " + statusCode);
            System.out.println("Response body: " + responseBody);

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }


    }
}
