package at.linuxlighthouse.api;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    public static void main(String[] args) {
        try {
            //IP-Adresse und Port des Server festlegen
            InetAddress inet = InetAddress.getByName("localhost");
            InetSocketAddress addr = new InetSocketAddress(inet, 4712);

            //Server anlegen
            //HttpServer server = HttpsServer.create(addr, 0);
            // TODO also possible with HTTPS ?
            HttpServer server = HttpServer.create(addr, 0);

            //Klasse für die Beantwortung der Client-Anfrage bekannt geben
            server.createContext("/", new Handler());

            //multithreaded Server starten
            server.setExecutor(Executors.newCachedThreadPool());
            server.start();
            System.out.println("LinuxLighthouse server started - to finish press Enter");

            //Server muss laufen
            System.in.read();
            System.out.println("LinuxLighhouse Server shut down");
            server.stop(0);
            ((ExecutorService) server.getExecutor()).shutdown();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
