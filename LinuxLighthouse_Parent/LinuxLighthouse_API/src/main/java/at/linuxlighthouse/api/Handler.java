package at.linuxlighthouse.api;


import at.linuxlighthouse.backend.Database;
import at.linuxlighthouse.classes.*;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import java.sql.SQLException;

public class Handler implements HttpHandler {


    /**
     * Handles the incoming HTTP request.
     *
     * @param exchange The HttpExchange object representing the request and response.
     * @throws IOException If an I/O error occurs while handling the request.
     */

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        // Determine the HTTP request method
        String requestMethod = exchange.getRequestMethod();

        // Get the URI of the requested resource
        URI uri = exchange.getRequestURI();
        System.out.println(uri + " " + requestMethod);
        String path = uri.getPath();

        if (requestMethod.equals("GET") && path.equals("/test")) {
            String response = "This is a test endpoint";
            setResponse(exchange, 200, response);
        }


        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        String[] paths = path.split("/");

        // Execute client request
        switch (requestMethod) {
            case "GET" -> get(exchange, paths);
            case "POST" -> post(exchange, paths);
            case "PUT" -> put(exchange, paths);
            case "DELETE" -> delete(exchange, paths);
            default -> setResponse(exchange, 400, new Notification("wrong HTTP request " + requestMethod).toXML());
        }
    }


    /**
     * Sets the HTTP response.
     *
     * @param exchange   The HttpExchange object representing the request and response.
     * @param statusCode The HTTP status code of the response.
     * @param xmlString  The XML string to be included in the response body.
     */
    private void setResponse(HttpExchange exchange, int statusCode, String xmlString) {
        if (xmlString.length() > 0) {
            xmlString = XMLHandler.XML_PROLOG_UTF8 + xmlString;
        }
        System.out.println("\tstatusCode = " + statusCode + "\trepsonsebody = '" + xmlString + "'");

        // Set the content type in the response body
        exchange.getResponseHeaders().set("Content-type", "text/plain; charset=UTF-8");

        // Set the response body and its length
        byte[] bytes = xmlString.getBytes(StandardCharsets.UTF_8);
        try {
            //HTTP DELETE braucht StatusCode 204; in dem Fall mus als Länge der mitgeschickten Daten -1 definiert werden
            // For HTTP DELETE requests, set the response body length to -1
            exchange.sendResponseHeaders(statusCode, statusCode != 204 ? bytes.length : -1);

            // Write the response body
            OutputStream os = exchange.getResponseBody();
            if (statusCode != 204) {
                os.write(bytes);
            }

            // Send the response
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Handles the HTTP GET request.
     *
     * @param exchange The HttpExchange object representing the request and response.
     * @param paths    An array of path segments from the request URI.
     */
    private void get(HttpExchange exchange, String[] paths) {
        String xmlAnswer = "";
        int statusCode = 200;

//        if (paths.length == 2 && paths[0].equals("admin")) {
//            // for path /admin/addpkg
//            if (paths[1].equals("addpkg")) {
//                //TODO ArrayList<Distribution> alDistros = Database.getDistros;
//            }

        if (paths[0].equals("readPackage")) {
            if (paths.length == 1) {
                try {
                    PackageManagerList pml = Database.getPackageManager();
                    xmlAnswer = pml.toXML();
                } catch (SQLException e) {
                    statusCode = 500;
                    xmlAnswer = new Notification(e.toString()).toXML();
                }
            }
        } else if (paths[0].equals("readDistros")) {
            if (paths.length == 1 ) {
                try {
                    DistributionList dl = Database.getDistros();
                    xmlAnswer = dl.toXML();
                } catch (SQLException e) {
                    statusCode = 500;
                    xmlAnswer = new Notification(e.toString()).toXML();
                }
            } else if (paths.length == 2) {
                int distroID = Integer.parseInt(paths[1]);
                try {
                    Distribution d = Database.getDistribution(distroID);
                    xmlAnswer = d.toXML();
                } catch (SQLException e) {
                    statusCode = 500;
                    xmlAnswer = new Notification(e.toString()).toXML();
                }
            }
        } else {
            statusCode = 400;
            xmlAnswer = new Notification("not found!").toXML();
        }
        setResponse(exchange, statusCode, xmlAnswer);

        /*

        int statusCode = 200; // Code für Fall das alles klappt
        String xmlAntwort = "";
        if (paths.length == 1 && paths[0].equals("weinliste")) {
            try {
                WeinList wl = Datenbank.leseWeine();
                xmlAntwort = wl.toXML();
            } catch (SQLException e) {
                statusCode = 500; // = INTERNAL SEVER ERROR
                xmlAntwort = new Meldung(e.toString()).toXML();
            }
        } else {
            statusCode = 400; // BAD REQUEST
            xmlAntwort = new Meldung("Falsche URI").toXML();
        }
        setResponse(exchange, statusCode, xmlAntwort);



        int statusCode = 200; // Code for successful response

        } catch (SQLException e) {
            statusCode = 500; // INTERNAL SERVER ERROR
            xmlAntwort = new Meldung(e.toString()).toXML();
        }
        } else {
            statusCode = 400; // BAD REQUEST
            xmlAntwort = new Meldung("Falsche URI").toXML();
        }
        setResponse(exchange, statusCode, xmlAntwort);



        private void get(HttpExchange exchange, String[] paths) {
    if (paths.length == 2 && paths[0].equals("api") && paths[1].equals("packages")) {
        // Handle the GET request for /api/packages
        // Retrieve the list of packages and send the response
    } else if (paths.length == 3 && paths[0].equals("api") && paths[1].equals("package")) {
        // Handle the GET request for /api/package/{id}
        // Retrieve details of the package with the given {id} and send the response
    } else {
        // If the requested path is not recognized, return a 404 Not Found response
        setResponse(exchange, 404, "Resource not found");
    }
}

         */
    }


    // TODO possibly here a good place for user authentification method
    //  and checking privileges?


    /**
     * Handles the HTTP POST request.
     *
     * @param exchange The HttpExchange object representing the request and response.
     * @param paths    An array of path segments from the request URI.
     */
    private void post(HttpExchange exchange, String[] paths) {
        String xmlAnswer = "";
        int statusCode = 201;

        if (paths.length == 2 && paths[0].equals("insertPackagemanager")) {
            addPackageManager(exchange);
        } else {   //HACK write better else clause
                statusCode = 400;
                xmlAnswer = new Notification("not found!").toXML();
        }

        setResponse(exchange, statusCode, xmlAnswer);

    }

    private void addPackageManager(HttpExchange exchange) {
        try {
            String xmlString = new String(exchange.getRequestBody().readAllBytes());
            PackageManager packageManager = new PackageManager(xmlString);
            Database.insertPackageManager(packageManager);
        } catch (IOException | SQLException e) {
            throw new RuntimeException(e);
        }
    }


//        private void createNewFinancialAccount(HttpExchange exchange) throws ServerException {
//            User currentUser = authenticate(exchange);
//            try {
//                String jsonString = new String(exchange.getRequestBody().readAllBytes());
//                FinancialAccount financialAccount = jsonb.fromJson(jsonString, FinancialAccount.class);
//                financialAccount.setOwner(currentUser);
//                //TODO for later: set user as Collaborator
//                Database.insertFinancialAccount(financialAccount);
//            } catch (IOException e) {
//                throw new ServerException(400, "Could not read request body", e);
//            }
//        }






    /**
     * Handles the HTTP PUT request.
     *
     * @param exchange The HttpExchange object representing the request and response.
     * @param paths    An array of path segments from the request URI.
     */
    private void put(HttpExchange exchange, String[] paths) {
        // TODO Logic for handling HTTP PUT requests
    }

    /**
     * Handles the HTTP DELETE request.
     *
     * @param exchange The HttpExchange object representing the request and response.
     * @param paths    An array of path segments from the request URI.
     */
    private void delete(HttpExchange exchange, String[] paths) {
        // TODO Logic for handling HTTP DELETE requests
    }
}
