package at.linuxlighthouse.classes;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

public class PackageManager {
    private int pkgId;
    private String pkgName;
    private ArrayList<Distribution> alDistros;  // BUG HACK cannot have both here
    private DistributionList distributions;

    public PackageManager() {
    }

    public PackageManager(int pkgId, String pkgName) {
        this.pkgId = pkgId;
        this.pkgName = pkgName;
    }

    public PackageManager(String xmlString) {
        if (xmlString == null || xmlString.length() == 0)
            return;
        SAXParserFactory spf = SAXParserFactory.newInstance();
        try {
            SAXParser sp = spf.newSAXParser();
            StringReader str = new StringReader(xmlString);
            XMLHandler xh = new XMLHandler();
            sp.parse(new InputSource(str), xh);
            pkgId = xh.getPackageManager().getPkgId();
            pkgName = xh.getPackageManager().getPkgName();
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new RuntimeException(e);
        }
    }


    public DistributionList getDistributions() {
        return distributions;
    }

    public void setDistributions(DistributionList distributions) {
        this.distributions = distributions;
    }

    public String getPkgName() {
        return pkgName;
    }

    public ArrayList<Distribution> getAlDistros() {
        return alDistros;
    }

    public void setPkgName(String pkgName) {
        this.pkgName = pkgName;
    }

    public void setAlDistros(ArrayList<Distribution> alDistros) {
        this.alDistros = alDistros;
    }

    public int getPkgId() {
        return pkgId;
    }

    public void setPkgId(int pkgId) {
        this.pkgId = pkgId;
    }

    public String toXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<packageManager>");
            sb.append("<pkgName>").append(pkgName).append("</pkgName>");
            sb.append("<pkgId>").append(pkgId).append("</pkgId>");
            // HACK Distos missing
            // TODO : add Distros but first change ArrayList<D> to DistroList
        sb.append("</packageManager>");

        return sb.toString();
    }


    @Override
    public String toString() {
        return "PackageManager{" +
                "pkgId=" + pkgId +
                ", pkgName='" + pkgName + '\'' +

                '}';
    }
}
