package at.linuxlighthouse.classes;

import java.time.LocalDateTime;

public class UserHistory {

    private int id;
    private User user;
    private LocalDateTime testDate;
    private Distribution rankOneDistro;
    private Distribution rankTwoDistro;
    private Distribution rankThreeDistro;
}
