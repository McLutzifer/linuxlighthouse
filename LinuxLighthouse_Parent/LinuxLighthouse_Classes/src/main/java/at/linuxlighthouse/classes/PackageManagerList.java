package at.linuxlighthouse.classes;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

public class PackageManagerList {

    private ArrayList<PackageManager> alPackageManager;


    public PackageManagerList(ArrayList<PackageManager> alPackageManager) {
        this.alPackageManager = alPackageManager;
    }

    public PackageManagerList(String xmlString) {
        if (xmlString == null || xmlString.length() == 0) {
            return;
        }

        // AHA SAX is a widely used event-driven XML parsing technology
        //  that allows you to process XML documents sequentially and efficiently
        //  without loading the entire document into memory.

        SAXParserFactory spf = SAXParserFactory.newInstance();
        try {
            SAXParser sp = spf.newSAXParser();
            StringReader str = new StringReader(xmlString);
            XMLHandler xh = new XMLHandler();
            sp.parse(new InputSource(str), xh);
            alPackageManager = xh.getAlPackageManager();
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String toXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<packageManagerList>");
        if (alPackageManager != null) {
            for (PackageManager p : alPackageManager) {
                sb.append(p.toXML());
            }
        }
        sb.append("</packageManagerList>");
        return sb.toString();
    }


    public ArrayList<PackageManager> getAlPackageManager() {
        return alPackageManager;
    }

    public void setAlPackageManager(ArrayList<PackageManager> alPackageManager) {
        this.alPackageManager = alPackageManager;
    }
}