package at.linuxlighthouse.classes;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

public class DistributionList {

    private ArrayList<Distribution> alDistribution;

    public DistributionList(ArrayList<Distribution> alDistribution) {
        this.alDistribution = alDistribution;
    }

    public DistributionList(String xmlString) {
       if (xmlString == null || xmlString.length() == 0) {
           return;
       }

        SAXParserFactory spf = SAXParserFactory.newInstance();
       try {
           SAXParser sp = spf.newSAXParser();
           StringReader str = new StringReader(xmlString);
           XMLHandler xh = new XMLHandler();
           sp.parse(new InputSource(str), xh);
           alDistribution = xh.getAlDistribution();
       } catch (ParserConfigurationException | IOException | SAXException e) {
           throw new RuntimeException(e);
       }
    }

    public String toXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<DistributionList>");
            if(alDistribution != null) {
                for (Distribution d : alDistribution) {
                    sb.append(d.toXML());
                }
            }
        sb.append("</DistributionList>");

        return sb.toString();
    }

    public ArrayList<Distribution> getAlDistribution() {
        return alDistribution;
    }

    public void setAlDistribution(ArrayList<Distribution> alDistribution) {
        this.alDistribution = alDistribution;
    }
}
