package at.linuxlighthouse.classes;

import java.util.ArrayList;

public class Distribution {
    private int distroId;
    private String distroName;

    //private ArrayList<Distribution> alMotherDistros;    -----> warum ArrayList??

    private int motherDistroID;

    // or ptivate ArrayList<Integer> and use distro ID's ?
    private ArrayList<DesktopEnvironment> alDefaultDE;
    private ArrayList<DesktopEnvironment> alSupportedDE;
    private ArrayList<PackageManager> alPackageManager;


    public Distribution() {
    }


    public Distribution(int distroId, String distroName, int motherDistroID) {
        this.distroId = distroId;
        this.distroName = distroName;
        this.motherDistroID = motherDistroID;
    }

    public Distribution(int distroId, String name, int motherDistroID, ArrayList<DesktopEnvironment> alDefaultDE, ArrayList<DesktopEnvironment> alSupportedDE, ArrayList<PackageManager> alPackageManager) {
        this.distroId = distroId;
        this.distroName = name;
        this.motherDistroID = motherDistroID;
        this.alDefaultDE = alDefaultDE;
        this.alSupportedDE = alSupportedDE;
        this.alPackageManager = alPackageManager;
    }

    public String getDistroName() {
        return distroName;
    }

    public int getDistroId() {
        return distroId;
    }

    public void setDistroId(int distroId) {
        this.distroId = distroId;
    }

    public void setDistroName(String distroName) {
        this.distroName = distroName;
    }

    public int getMotherDistroID() {
        return motherDistroID;
    }

    public void setMotherDistroID(int motherDistroID) {
        this.motherDistroID = motherDistroID;
    }

    public String toXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<Distribution>");
            sb.append("<distroId>").append(distroId).append("</distroId>");
            sb.append("<distroName>").append(distroName).append("</distroName>");
            sb.append("<motherDistroID>").append(motherDistroID).append("</motherDistroID>");
            // TODO add the other variables
        sb.append("</Distribution>");

        return sb.toString();
    }

    @Override
    public String toString() {
        return "Distribution{" +
                "distroId=" + distroId +
                ", distroName='" + distroName + '\'' +
                ", motherDistroID=" + motherDistroID +
                ", alDefaultDE=" + alDefaultDE +
                ", alSupportedDE=" + alSupportedDE +
                ", alPackageManager=" + alPackageManager +
                '}';
    }
}
