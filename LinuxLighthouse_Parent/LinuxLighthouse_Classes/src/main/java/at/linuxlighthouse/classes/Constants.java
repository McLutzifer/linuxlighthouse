package at.linuxlighthouse.classes;

public class Constants {

    //DesktopEnvironment
    protected static final String DE_TABLE = "DesktopEnvironment";
    protected static final String DE_ID = "de_id";
    protected static final String DE_NAME ="de_name";
    protected static final String DE_IS_A_DE = "is_a_de";
    protected static final String DE_RESOURCE = "resource_heavy";

    // Distributions
    public static final String DISTRO_TABLE = "Distributions";
    public static final String DISTRO_ID = "distro_id";
    public static final String DISTRO_NAME = "distro_name";
    public static final String DISTRO_MOTHER = "mother_distro_id";

    // DistroDefaultDE
    // Foreign keys: distro_id     de_id
    protected static final String DISTRO_DEFAULT_DE_TABLE = "DistroDefaultDE";


    // DistroPkg
    // Foreign keys: distro_id	pkg_id
    public static final String DISTRO_PKG_TABLE = "DistroPkg";


    // DistroSupportedDE
    // Foreign keys: distro_id	de_id
    protected static final String DISTRO_SUPPORTED_DE_TABLE = "DistroSupportedDE";


    //Features
    // Foreign keys:	question_id 	distro_id
    protected static final String FEATURES_TABLE = "Features";
    protected static final String FEATURES_ID = "feature_id";
    protected static final String FEATURES_VALUE = "value";


    // Packagemanager
    public static final String PACKAGEMANAGER_TABLE = "Packagemanager";
    public static final String PACKAGEMANAGER_NAME = "pkg_name";
    public static final String PACKAGEMANAGER_ID = "pkg_id";

    // Passwords
    // Foreign keys:	user_id
    protected static final String PASSWORDS_TABLE = "Passwords";
    protected static final String PASSWORDS_ID = "pwd_id";
    protected static final String PASSWORD_HASH = "password_hash";

    // Questions
    protected static final String QUESTIONS_TABLE = "Questions";
    protected static final String QUESTION_ID = "question_id";
    protected static final String QUESTION_TEXT = "question_text";
    protected static final String QUESTION_DELETED = "deleted";


    // UserComments
    // Foreign keys: user_id	distro_id
    protected static final String USERCOMMENTS_TABLE = "UserComments";
    protected static final String USERCOMMENTS_ID = "comment_id";
    protected static final String USERCOMMENTS_COMMENT = "comment";
    protected static final String USERCOMMENTS_DATE = "comment_date";

    // Usernames
    protected static final String USERNAMES_TABLE = "Usernames";
    protected static final String USERNAMES_ID = "user_id";
    protected static final String USERNAMES_NAMES = "username";
    protected static final String USERNAMES_ADMIN_RIGHTS = "admin_rights";

    // Votings
    // Foreign keys:	user_id	distro_id
    protected static final String VOTINGS_TABLE = "Votings";
    protected static final String VOTINGS_ID = "voting_id";
}
