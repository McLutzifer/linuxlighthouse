package at.linuxlighthouse.classes;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;


public class XMLHandler extends DefaultHandler {

    public static final String XML_PROLOG_UTF8 = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";

    private String text;
    private Notification notification;

    private Distribution distribution;
    private ArrayList<Distribution> alDistribution;

    private ArrayList<PackageManager> alPackageManager;
    private PackageManager packageManager;


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        //qName is name of starting element
        switch(qName.toLowerCase()) {
            case "packagemanagerlist" -> alPackageManager = new ArrayList<>();
            case "packagemanager" -> packageManager = new PackageManager();
            case "distribution" -> distribution = new Distribution();
            case "distributionlist" -> alDistribution = new ArrayList<>();
            case "notification" -> notification = new Notification();
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

        switch (qName.toLowerCase()) {
            //packagemanager
            case "pkgid" -> packageManager.setPkgId(Integer.parseInt(text));
            case "pkgname" -> packageManager.setPkgName(text);
            case "packagemanager" -> {
                if(alPackageManager != null) {
                    alPackageManager.add(packageManager);
                }
            }
            // Distributions
            case "distroname" -> distribution.setDistroName(text);
            case "distribution" -> {
                if(alDistribution != null) {
                    alDistribution.add(distribution);
                }
            }

            case "text" -> notification.setText(text);
        }


    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        text = new String(ch, start, length);
    }

    public Distribution getDistribution() {
        return distribution;
    }

    public ArrayList<Distribution> getAlDistribution() {
        return alDistribution;
    }

    public PackageManager getPackageManager() {
        return packageManager;
    }

    public ArrayList<PackageManager> getAlPackageManager() {
        return alPackageManager;
    }

    public Notification getNotification() {
        return notification;
    }

}
