package at.linuxlighthouse.classes;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.StringReader;

public class Notification {

    private String text;

    public Notification() {}

    public Notification(String text) {
        if (text == null || text.length() == 0) {
            return;
        }
        if (text.startsWith("<")) {
            this.text = text;
        } else {
            SAXParserFactory spf = SAXParserFactory.newInstance();
            try {
                SAXParser sp = spf.newSAXParser();
                StringReader str = new StringReader(text);
                XMLHandler xh = new XMLHandler();
                sp.parse(new InputSource(str), xh);
                text = xh.getNotification().getText();
            } catch (ParserConfigurationException | SAXException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String toXML() {
        return "<notification><text>" + text + "</text></notification>";
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
