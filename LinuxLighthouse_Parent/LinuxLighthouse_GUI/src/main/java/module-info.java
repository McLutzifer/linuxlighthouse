module LinuxLighthouse.GUI {
    requires javafx.graphics;
    requires javafx.controls;
    requires java.net.http;
    requires LinuxLighthouse.Classes;
    requires javafx.fxml;

    exports at.linuxlighthouse.gui;
    opens at.linuxlighthouse.gui to javafx.fxml;
}