package at.linuxlighthouse.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

public class StartPageController {

    public Button testButton2;
    @FXML
    private Button testButton;

    @FXML
    private Button btnAdminPanel;

    @FXML
    public void startTest(ActionEvent actionEvent) {
        Optional<ButtonType> test = new Alert(Alert.AlertType.WARNING).showAndWait();
    }


    @FXML
    public void testAlert(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("TEST ALERT");
        alert.setHeaderText(null);
        alert.setContentText("This is a testy test alert message!");

        alert.showAndWait();
    }

    @FXML
    public void toAdminPanel(ActionEvent actionEvent) {

        try {
            FXMLLoader loader = new FXMLLoader();
            Parent adminPanelRoot = loader.load(getClass().getResource("/AdminPanel.fxml").openStream());

            Stage currentStage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            currentStage.getScene().setRoot(adminPanelRoot);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    public void testAlert2(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning!!!");
        alert.setHeaderText("Importante!!");
        alert.setContentText("Never press the second Alert");

        alert.showAndWait();
    }
}
