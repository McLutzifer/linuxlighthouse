package at.linuxlighthouse.gui;

import at.linuxlighthouse.classes.Distribution;
import at.linuxlighthouse.classes.DistributionList;
import at.linuxlighthouse.classes.PackageManager;
import at.linuxlighthouse.classes.PackageManagerList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class AdminPanelController {

    @FXML
    private Button readDistros;

    @FXML
    private Button readPkg;

    @FXML
    private Button btnAddPackageManager;

    @FXML
    private Button btnAddDistro;

    @FXML
    private Button btnAddQuestion;

    @FXML
    private Button btnBackToStart;

    @FXML
    private Button btnDeleteComment;

    @FXML
    private Button btnDeleteDistro;

    @FXML
    private Button btnDeleteQuestion;

    @FXML
    private Button btnStatistics;

    @FXML
    private Button btnUpdateDistro;

    @FXML
    void showSuccess(ActionEvent event) {

    }

    @FXML
    public void openAddPackageManager(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/AddPackageManager.fxml"));
            Parent dialogRoot = loader.load();

            Stage dialogStage = new Stage();
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.setScene(new Scene(dialogRoot));
            dialogStage.setTitle("Add Packagemanager");
            dialogStage.showAndWait();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    private void readPackageManager(ActionEvent actionEvent) {
        try {
            String s = ServiceFunctions.get("readPackage", null);
            PackageManagerList pml = new PackageManagerList(s);

            // NOTE im Server Terminal: /readPackage GET

            System.out.println("All good until here Part 1");
            System.out.println("Size of pml: " + pml.getAlPackageManager().size());

            for (PackageManager p : pml.getAlPackageManager()) {
                System.out.println("now we loop");
                System.out.println(p.toString());
            }

            if (pml.getAlPackageManager().size() == 0) {
                System.out.println("size zero");
                System.out.println("no packagemanager yet");
            }

            System.out.println("We done");
        } catch (LighthouseException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    private void readDistributions(ActionEvent actionEvent) {
        try{
            String s = ServiceFunctions.get("readDistros", null);
            DistributionList dl = new DistributionList(s);

            for(Distribution d : dl.getAlDistribution()) {
                System.out.println(d.toString());
            }

        } catch (LighthouseException e) {
            throw new RuntimeException(e);
        }
    }
}
