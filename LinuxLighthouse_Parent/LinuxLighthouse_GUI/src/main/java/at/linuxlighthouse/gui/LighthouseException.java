package at.linuxlighthouse.gui;

import java.io.Serial;

public class LighthouseException extends Exception{

    @Serial
    private static final long serialVersionUID = -4498460196038928348L;

    public LighthouseException(String text) {
        super(text);
    }
}
