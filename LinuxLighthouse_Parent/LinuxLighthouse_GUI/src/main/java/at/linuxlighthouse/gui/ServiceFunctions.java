package at.linuxlighthouse.gui;

import at.linuxlighthouse.classes.Notification;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class ServiceFunctions {
    private static final String SERVER_URI = "http://localhost:4712/";

    public static String get(String item, String id) throws LighthouseException {
        String uriString = SERVER_URI + item;
        if (id != null && id.length() > 0) {
            uriString += "/" + id;
        }

        try {
            URI uri = new URI(uriString);
            //Client anlegen
            HttpClient client = HttpClient.newHttpClient();
            //Anfrage an den Server vorbereiten
            HttpRequest request = HttpRequest.newBuilder(uri).GET().build();
            //Anfrage absenden und auf Antwort warten
            HttpResponse<byte[]> response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());
            // Antwort analysieren
            int statusCode = response.statusCode();
            String line = new String(response.body());
            if (statusCode == 200) {
                return line;
            } else {
                throw new LighthouseException(new Notification(line).getText());
            }
        } catch (URISyntaxException | IOException | InterruptedException e) {
            throw new LighthouseException(e.toString());
        }
    }


    public static void post(String path, String xmlString) throws LighthouseException {
        String uriString = SERVER_URI + path;
        try {
            URI uri = new URI(uriString);
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder(uri)
                        .POST(HttpRequest.BodyPublishers.ofString(xmlString))
                        .build();

            HttpResponse<byte[]> response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());
            int statusCode = response.statusCode();
            if (statusCode != 201) {
                String xmlResponse = new String(response.body());

            }
        } catch (URISyntaxException | IOException | InterruptedException e) {
            throw new LighthouseException(e.toString());
        }
    }

    public static void put(String path, String xmlString) throws LighthouseException {
        String uriString = SERVER_URI + path;
        try {
            URI uri = new URI(uriString);
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder(uri)
                    .PUT(HttpRequest.BodyPublishers.ofString(xmlString))
                    .build();

            HttpResponse<byte[]> response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());
            int statusCode = response.statusCode();
            if (statusCode != 200) {
                String xmlResponse = new String(response.body());

            }
        } catch (URISyntaxException | IOException | InterruptedException e) {
            throw new LighthouseException(e.toString());
        }
    }

    /* TODO
    public static void delete(String path, String xmlString) throws LighthouseException {
        String uriString = SERVER_URI + path;
        try {
            URI uri = new URI(uriString);
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder(uri)
                    .PUT(HttpRequest.BodyPublishers.ofString(xmlString))
                    .build();

            HttpResponse<byte[]> response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());
            int statusCode = response.statusCode();
            if (statusCode != 200) {
                String xmlResponse = new String(response.body());

            }
        } catch (URISyntaxException | IOException | InterruptedException e) {
            throw new LighthouseException(e.toString());
        }
    }

     */


}
