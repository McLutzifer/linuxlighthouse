package at.linuxlighthouse.gui;

import at.linuxlighthouse.classes.Distribution;
import at.linuxlighthouse.classes.DistributionList;
import at.linuxlighthouse.classes.PackageManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.util.ArrayList;
import java.util.List;

public class AddPackageManagerController {
    @FXML
    private ListView<Distribution> distributionListView;
    @FXML
    private Button btnCancel;
    @FXML
    private Button btnSave;
    @FXML
    private TextField packageManagerTextField;

    // TODO listview get request to get distributions

    //PackageManager testPkg = new PackageManager(0, "Testname");


    @FXML
    private void handleSaveButtonAction(ActionEvent actionEvent) throws LighthouseException {

        // TODO if textfield is empty : ERROR

        PackageManager packageManager = new PackageManager();
        packageManager.setPkgName(packageManagerTextField.getText());
        packageManager.setPkgId(0);
        DistributionList selectedDistributions = new DistributionList((ArrayList<Distribution>) distributionListView.getSelectionModel().getSelectedItems());
        packageManager.setDistributions(selectedDistributions);

        ServiceFunctions.post("insertPackagemanager", packageManager.toXML());
    }
}
