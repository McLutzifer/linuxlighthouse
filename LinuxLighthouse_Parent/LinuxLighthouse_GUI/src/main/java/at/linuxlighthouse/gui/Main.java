package at.linuxlighthouse.gui;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;

import java.io.IOException;

public class Main extends Application {

    private Pane startPage;
    private Pane adminPage;
    private Pane currentPage;


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/StartPage.fxml"));
        Parent root = loader.load();
        StartPageController controller = loader.getController();

        Scene scene = new Scene(root);

        //TODO add CSS ?
        scene.getStylesheets().add("at/linuxlighthouse/gui/firstTest.css");

        //scene.getStylesheets().add(getClass().getResource("at/linuxlighthouse/gui/firstTest.css").toExternalForm());

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
