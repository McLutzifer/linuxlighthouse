package at.linuxlighthouse.backend;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DefaultAdminSetup {

    private Connection connection;

    public DefaultAdminSetup(Database database) {
//        this.connection = database.getConnection();
//
//        String username = "admin";
//        String password = "adminpassword";
//
//        // Hash the password
//        String hashedPassword = PasswordHashing.hashPassword(password);
//
//        // Insert the admin user into the database
//        String sql = "INSERT INTO Usernames (username, admin_rights) VALUES (?, ?)";
//        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
//            stmt.setString(1, username);
//            stmt.setBoolean(2, true);
//            stmt.executeUpdate();
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }

        // TODO check both sql commants up and down

        // Insert the hashed password into the passwords table
//        sql = "INSERT INTO Passwords (user_id, password_hash) VALUES (LAST_INSERT_ID(), ?)";
//        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
//            stmt.setString(1, hashedPassword);
//            stmt.executeUpdate();
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }

    }
}





/*

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AdminSetup {
    private static final String DB_URL = "jdbc:mariadb://localhost:3306/your_database_name";
    private static final String DB_USER = "your_username";
    private static final String DB_PASSWORD = "your_password";

    public static void main(String[] args) {
        try (Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {
            String username = "admin";
            String password = "adminpassword"; // Replace with the desired password

            // Hash the password
            String hashedPassword = PasswordUtil.hashPassword(password);

            // Insert the admin user into the database
            String sql = "INSERT INTO Usernames (username, admin_rights) VALUES (?, ?)";
            try (PreparedStatement stmt = conn.prepareStatement(sql)) {
                stmt.setString(1, username);
                stmt.setBoolean(2, true);
                stmt.executeUpdate();
            }

            // Insert the hashed password into the passwords table
            sql = "INSERT INTO Passwords (user_id, password_hash) VALUES (LAST_INSERT_ID(), ?)";
            try (PreparedStatement stmt = conn.prepareStatement(sql)) {
                stmt.setString(1, hashedPassword);
                stmt.executeUpdate();
            }

            System.out.println("Default admin user created successfully.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}


 */