package at.linuxlighthouse.backend;

import org.mindrot.jbcrypt.BCrypt;


/**
 * Utility class for hashing passwords using the BCrypt algorithm.
 */

public class PasswordHashing {

    /**
     * Generates a hash for the given password.
     *
     * @param password the password to be hashed
     * @return the hashed password
     */
    public static String hashPassword(String password) {
        String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt());
        return hashedPassword;
    }

    /**
     * Verifies if the provided password matches the given hash.
     *
     * @param password     the password to be verified
     * @param hashedPassword the hashed password to compare against
     * @return true if the password matches the hash, false otherwise
     */
    public static boolean verifyPassword(String password, String hashedPassword) {
        return BCrypt.checkpw(password, hashedPassword);
    }
}
