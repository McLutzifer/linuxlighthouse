package at.linuxlighthouse.backend;


import at.linuxlighthouse.classes.Distribution;
import at.linuxlighthouse.classes.DistributionList;
import at.linuxlighthouse.classes.PackageManager;
import at.linuxlighthouse.classes.PackageManagerList;

import java.sql.*;
import java.util.ArrayList;

import static at.linuxlighthouse.classes.Constants.*;

public class Database {

    // config file so I don't have to hardcode my password as a String
    // for security reasons config.properties is added to .gitignore
    private static final String CONFIG_FILE_PATH = "/home/lukas/Documents/GitLab/linuxlighthouse/LinuxLighthouse_Parent/LinuxLighthouse_Backend/src/main/java/at/linuxlighthouse/backend/config.properties";

    private static Connection connection;

    // HACK the comment below is the original static initializer
    /* TODO here I put my DB credentials in a separate config file so I don't have to hardcode my password etc but it's not working and I will come back to it later
   static {
        Properties properties = new Properties();

            try (InputStream configFile = Database.class.getClassLoader().getResourceAsStream(CONFIG_FILE_PATH)) {
            //try (FileInputStream configFile = new FileInputStream(CONFIG_FILE_PATH)) {   // FIXME - why not FileInputStream

                properties.load(configFile);

                String dbUrl = properties.getProperty("db.url");
                String dbUser = properties.getProperty("db.user");
                String dbPassword = properties.getProperty("db.password");

                connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
            } catch (SQLException | IOException e) {
                System.out.println("FILE STILL NOT FOUND");
                throw new RuntimeException(e);
            }
        }
*/

    private static final String DB_URL = "jdbc:mariadb://localhost:3306/linuxlighthouse";
    private static final String DB_USER = "lukas";
    private static final String DB_PASSWORD = "yX4vdWpNPnkNy3";

    static {
        try {
            connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
        } catch (SQLException e) {
            System.out.println("Failed to establish the database connection!");
            e.printStackTrace();
        }
    }


    public static DistributionList getDistros() throws SQLException {
        String str = String.format("SELECT * FROM %s", DISTRO_TABLE);
        ArrayList<Distribution> alDistros = new ArrayList<>();

        try (PreparedStatement stmt = connection.prepareStatement(str)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                alDistros.add(new Distribution(rs.getInt(DISTRO_ID), rs.getString(DISTRO_NAME), rs.getInt(DISTRO_MOTHER), null, null, null));
            }
            rs.close();
            return new DistributionList(alDistros);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static Distribution getDistribution(int id) throws SQLException {
        String str = String.format("SELECT * FROM %s WHERE %s = %d ", DISTRO_TABLE, DISTRO_ID, id);
        try (PreparedStatement stmt = connection.prepareStatement(str)) {
            ResultSet rs = stmt.executeQuery();
            Distribution d = new Distribution(rs.getInt(DISTRO_ID), rs.getString(DISTRO_NAME), rs.getInt(DISTRO_MOTHER));
            rs.close();
            return d;
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }


    public static PackageManagerList getPackageManager() throws SQLException {
        return getPackageManager(0);
    }


    public static PackageManagerList getPackageManager(int DistroID) throws SQLException {
        String insert;

        if (DistroID > 0) {
//            Distribution d = getDistribution(DistroID);
//            String distroName = d.getDistroName();

            insert = String.format("SELECT %s.%s, %s.%s " +
                            "FROM %s " +
                            "JOIN %s ON %s.%s = %s.%s " +
                            "WHERE %s.%s = ?;",
                    PACKAGEMANAGER_TABLE, PACKAGEMANAGER_ID,
                    PACKAGEMANAGER_TABLE, PACKAGEMANAGER_NAME,
                    DISTRO_PKG_TABLE,
                    PACKAGEMANAGER_TABLE, DISTRO_PKG_TABLE, PACKAGEMANAGER_ID,
                    PACKAGEMANAGER_TABLE, PACKAGEMANAGER_ID,
                    DISTRO_PKG_TABLE, DISTRO_ID);
                    // SELECT p.pkg_id, p.pkg_name FROM DistroPkg dp JOIN Packagemanager p ON dp.pkg_id = p.pkg_id WHERE dp.distro_id = distro_id;
        } else {
            insert = String.format("SELECT * FROM %s", PACKAGEMANAGER_TABLE);
        }

        ArrayList<PackageManager> alPkgManager = new ArrayList<>();

        try (PreparedStatement stmt = connection.prepareStatement(insert)) {
            if (DistroID > 0) {
                stmt.setInt(1, DistroID);
            }
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                // leave out ArrayList<Distribution> alDistros in constructor for now.... TODO
                alPkgManager.add(new PackageManager(rs.getInt(PACKAGEMANAGER_ID), rs.getString(PACKAGEMANAGER_NAME)));
            }
            rs.close();
            return new PackageManagerList(alPkgManager);

        } catch (SQLException e) {
            throw e;
        }
    }


    public static void insertPackageManager(PackageManager packageManager) throws SQLException{

        String insert = String.format("INSERT INTO %s (%s) VALUES(?)", PACKAGEMANAGER_TABLE, PACKAGEMANAGER_NAME);

        try (PreparedStatement stmt = connection.prepareStatement(insert)) {
            stmt.setString(1, packageManager.getPkgName());

            // retrieving the ID of the Pckgmg I'm inserting right now
            ResultSet generatedKeys = stmt.getGeneratedKeys();
            // The keys returned are specific to the row(s) that were inserted by the most recent execution of the PreparedStatement.

            if (generatedKeys.next()) {
                int pkgId = generatedKeys.getInt(1);

                if (packageManager.getAlDistros().size() != 0) {
                    for (Distribution d: packageManager.getAlDistros()) {
                        insertDistroPkg(d, pkgId);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insertDistribution(Distribution distribution) throws SQLException{
        String insert = String.format("INSERT INTO %s (%s, %s) VALUES(?, ?)", DISTRO_TABLE, DISTRO_NAME, DISTRO_MOTHER);
        try (PreparedStatement stmt = connection.prepareStatement(insert)) {
            stmt.setString(1, distribution.getDistroName());
            stmt.setInt(2, distribution.getMotherDistroID());

            ResultSet generatedKeys = stmt.getGeneratedKeys();

            if (generatedKeys.next()) {
                int pkgdistroId = generatedKeys.getInt(1);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insertDistroPkg(Distribution distro, int pkgId) throws SQLException {

        // TODO - probably Distro ID to receive like that
        //String name = distro.getName();
        //String str = String.format("SELECT %s FROM %s WHERE %s = %s ", DISTRO_ID, DISTRO_TABLE, DISTRO_NAME, name);

        String insert = String.format("INSERT INTO %s (%s, %s) VALUES (?, ?)", DISTRO_PKG_TABLE, DISTRO_ID, PACKAGEMANAGER_ID);

        try (PreparedStatement stmt = connection.prepareStatement(insert)) {
            stmt.setInt(1, distro.getDistroId());
            stmt.setInt(2, pkgId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /*
    public void insertDistribution(Distribution distribution) {
        String insert = "INSERT INTO Distributions (distro_name, mother_distro_id) VALUES (?, ?)";

        try (PreparedStatement stmt = connection.prepareStatement(insert)) {
            stmt.setString(1, distribution.getName());
            stmt.setInt(2, distribution.getMotherDistroId());
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertQuestion(Question question) {
        String insert = "INSERT INTO Questions (question_text) VALUES (?)";

        try (PreparedStatement stmt = connection.prepareStatement(insert)) {
            stmt.setString(1, question.getQuestionText());
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertFeature(Feature feature) {
        String insert = "INSERT INTO Features (question_id, distro_id, value) VALUES (?, ?, ?)";

        try (PreparedStatement stmt = connection.prepareStatement(insert)) {
            stmt.setInt(1, feature.getQuestion().getQuestionId());
            stmt.setInt(2, feature.getDistro().getDistroId());
            stmt.setInt(3, feature.getValue());
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
     */


    //    // Distributions
    //    private static final String DISTRO_TABLE = "Distributions";
    //    private static final String DISTRO_ID = "distro_id";
    //    private static final String DISTRO_NAME = "distro_name";
    //    private static final String DISTRO_MOTHER = "mother_distro_id";

    //    // DistroPkg
    //    // Foreign keys: distro_id	pkg_id
    //    private static final String DISTRO_PKG_TABLE = "DistroPkg";

    //    // Packagemanager
    //    private static final String PACKAGEMANAGER_TABLE = "Packagemanager";
    //    private static final String PACKAGEMANAGER_NAME = "pkg_name";
    //    private static final String PACKAGEMANAGER_ID = "pkg_id";


    public static void insertDistro(Distribution distro) throws SQLException {    }
    /*
CREATE TABLE Distributions (
	distro_id INT AUTO_INCREMENT PRIMARY KEY,
	distro_name VARCHAR(50) NOT NULL UNIQUE,
	mother_distro_id INT NOT NULL
);
     */
    /*
    * public static void insertWein(Wein wein) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        String insert = "INSERT INTO " + WEIN_TABLE + " VALUES(" +
                "DEFAULT, " +
                "?," + // NAME
                "?," + // DESCR
                "?)";  // PREIS

        String insert2 = String.format("INSERT INTO %s VALUES( DEFAULT, ?, ?, ?)", WEIN_TABLE);

        try {
            conn = DriverManager.getConnection(CONNECTION);
            stmt = conn.prepareStatement(insert);
            stmt.setString(1, wein.getName());
            stmt.setString(2, wein.getDesc());
            stmt.setDouble(3, wein.getPreis());
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            }
            catch (SQLException e) {
                throw e;
            }
        }
    }
    * */




}
