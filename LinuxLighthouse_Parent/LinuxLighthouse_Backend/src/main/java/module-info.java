module LinuxLighthouse.Backend {
    exports at.linuxlighthouse.backend;
    requires LinuxLighthouse.Classes;
    requires java.sql;
    requires jbcrypt;

}