
DROP TABLE DistroPkg

DROP TABLE DistroDefaultDE 

DROP TABLE DistroSupportedDE 

DROP TABLE DesktopEnvironment

DROP TABLE DistroPkg

DROP TABLE Packagemanager 

DROP TABLE Passwords 

DROP TABLE UserHistory

DROP TABLE UserComments

DROP TABLE Votings

DROP TABLE Usernames 

DROP TABLE Derivatives 

DROP TABLE Features

DROP TABLE Questions

DROP TABLE Distributions





CREATE TABLE Usernames (
  user_id INT AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(255) NOT NULL UNIQUE,
  admin_rights BOOLEAN NOT NULL
);


CREATE TABLE Passwords (
  pwd_id INT AUTO_INCREMENT PRIMARY KEY,
  user_id INT NOT NULL,
  password_hash VARCHAR(255) NOT NULL,
  
  FOREIGN KEY (user_id) REFERENCES Usernames(user_id)
);


CREATE TABLE Questions (
	question_id INT AUTO_INCREMENT PRIMARY KEY,
	question_text VARCHAR(250),
	deleted BOOLEAN DEFAULT FALSE
);


CREATE TABLE Distributions (
	distro_id INT AUTO_INCREMENT PRIMARY KEY,
	distro_name VARCHAR(50) NOT NULL UNIQUE,
	mother_distro_id INT NOT NULL
);

CREATE TABLE Features (
	feature_id INT AUTO_INCREMENT PRIMARY KEY,
	question_id INT NOT NULL,
	distro_id INT NOT NULL,
	value INT NOT NULL,
	
	FOREIGN KEY (question_id) REFERENCES Questions(question_id),
	FOREIGN KEY (distro_id) REFERENCES Distributions(distro_id)
);


CREATE TABLE Votings (
	voting_id INT AUTO_INCREMENT PRIMARY KEY,
	user_id INT NOT NULL,
	distro_id INT NOT NULL,
	
	FOREIGN KEY (user_id) REFERENCES Usernames(user_id),
	FOREIGN KEY (distro_id) REFERENCES Distributions(distro_id)
);


CREATE TABLE UserComments (
	comment_id INT AUTO_INCREMENT PRIMARY KEY,
	user_id INT NOT NULL,
	distro_id INT NOT NULL,
	comment VARCHAR(250) NOT NULL,
	comment_date DATE NOT NULL,
	
	FOREIGN KEY (user_id) REFERENCES Usernames(user_id),
	FOREIGN KEY (distro_id) REFERENCES Distributions(distro_id)
);

CREATE TABLE DesktopEnvironment (
	de_id INT AUTO_INCREMENT PRIMARY KEY,
	de_name VARCHAR(50) NOT NULL,
	is_a_de BOOLEAN NOT NULL,
	resource_heavy BOOLEAN NOT NULL
);


CREATE TABLE Packagemanager (
	pkg_id INT AUTO_INCREMENT PRIMARY KEY,
	pkg_name VARCHAR(50) NOT NULL
);


CREATE TABLE DistroPkg (
	distro_id INT NOT NULL,
	pkg_id INT NOT NULL,
	
	PRIMARY KEY (distro_id, pkg_id),
	
	FOREIGN KEY (distro_id) REFERENCES Distributions (distro_id),
	FOREIGN KEY (pkg_id) REFERENCES Packagemanager (pkg_id)
);


CREATE TABLE DistroDefaultDE (
	distro_id INT NOT NULL,
	de_id INT NOT NULL,
	
	PRIMARY KEY (distro_id, de_id),
	
	FOREIGN KEY (distro_id) REFERENCES Distributions (distro_id),
	FOREIGN KEY (de_id) REFERENCES DesktopEnvironment (de_id)
);


CREATE TABLE DistroSupportedDE (
	distro_id INT NOT NULL,
	de_id INT NOT NULL,
	
	PRIMARY KEY (distro_id, de_id),
	
	FOREIGN KEY (distro_id) REFERENCES Distributions (distro_id),
	FOREIGN KEY (de_id) REFERENCES DesktopEnvironment (de_id)
);

