-- phpMyAdmin SQL Dump
-- version 5.2.1-1.fc38
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 13, 2023 at 03:27 PM
-- Server version: 10.5.20-MariaDB
-- PHP Version: 8.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `linuxlighthouse`
--

-- --------------------------------------------------------

--
-- Table structure for table `DesktopEnvironment`
--

CREATE TABLE `DesktopEnvironment` (
  `de_id` int(11) NOT NULL,
  `de_name` varchar(50) NOT NULL,
  `is_a_de` tinyint(1) NOT NULL,
  `resource_heavy` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Distributions`
--

CREATE TABLE `Distributions` (
  `distro_id` int(11) NOT NULL,
  `distro_name` varchar(50) NOT NULL,
  `mother_distro_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `DistroDefaultDE`
--

CREATE TABLE `DistroDefaultDE` (
  `distro_id` int(11) NOT NULL,
  `de_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `DistroPkg`
--

CREATE TABLE `DistroPkg` (
  `distro_id` int(11) NOT NULL,
  `pkg_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `DistroSupportedDE`
--

CREATE TABLE `DistroSupportedDE` (
  `distro_id` int(11) NOT NULL,
  `de_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Features`
--

CREATE TABLE `Features` (
  `feature_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `distro_id` int(11) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Packagemanager`
--

CREATE TABLE `Packagemanager` (
  `pkg_id` int(11) NOT NULL,
  `pkg_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Passwords`
--

CREATE TABLE `Passwords` (
  `pwd_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `password_hash` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Questions`
--

CREATE TABLE `Questions` (
  `question_id` int(11) NOT NULL,
  `question_text` varchar(250) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `UserComments`
--

CREATE TABLE `UserComments` (
  `comment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `distro_id` int(11) NOT NULL,
  `comment` varchar(250) NOT NULL,
  `comment_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Usernames`
--

CREATE TABLE `Usernames` (
  `user_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `admin_rights` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Votings`
--

CREATE TABLE `Votings` (
  `voting_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `distro_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `DesktopEnvironment`
--
ALTER TABLE `DesktopEnvironment`
  ADD PRIMARY KEY (`de_id`);

--
-- Indexes for table `Distributions`
--
ALTER TABLE `Distributions`
  ADD PRIMARY KEY (`distro_id`),
  ADD UNIQUE KEY `distro_name` (`distro_name`);

--
-- Indexes for table `DistroDefaultDE`
--
ALTER TABLE `DistroDefaultDE`
  ADD PRIMARY KEY (`distro_id`,`de_id`),
  ADD KEY `de_id` (`de_id`);

--
-- Indexes for table `DistroPkg`
--
ALTER TABLE `DistroPkg`
  ADD PRIMARY KEY (`distro_id`,`pkg_id`),
  ADD KEY `pkg_id` (`pkg_id`);

--
-- Indexes for table `DistroSupportedDE`
--
ALTER TABLE `DistroSupportedDE`
  ADD PRIMARY KEY (`distro_id`,`de_id`),
  ADD KEY `de_id` (`de_id`);

--
-- Indexes for table `Features`
--
ALTER TABLE `Features`
  ADD PRIMARY KEY (`feature_id`),
  ADD KEY `question_id` (`question_id`),
  ADD KEY `distro_id` (`distro_id`);

--
-- Indexes for table `Packagemanager`
--
ALTER TABLE `Packagemanager`
  ADD PRIMARY KEY (`pkg_id`);

--
-- Indexes for table `Passwords`
--
ALTER TABLE `Passwords`
  ADD PRIMARY KEY (`pwd_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `Questions`
--
ALTER TABLE `Questions`
  ADD PRIMARY KEY (`question_id`);

--
-- Indexes for table `UserComments`
--
ALTER TABLE `UserComments`
  ADD PRIMARY KEY (`comment_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `distro_id` (`distro_id`);

--
-- Indexes for table `Usernames`
--
ALTER TABLE `Usernames`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `Votings`
--
ALTER TABLE `Votings`
  ADD PRIMARY KEY (`voting_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `distro_id` (`distro_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `DesktopEnvironment`
--
ALTER TABLE `DesktopEnvironment`
  MODIFY `de_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Distributions`
--
ALTER TABLE `Distributions`
  MODIFY `distro_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Features`
--
ALTER TABLE `Features`
  MODIFY `feature_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Packagemanager`
--
ALTER TABLE `Packagemanager`
  MODIFY `pkg_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Passwords`
--
ALTER TABLE `Passwords`
  MODIFY `pwd_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Questions`
--
ALTER TABLE `Questions`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `UserComments`
--
ALTER TABLE `UserComments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Usernames`
--
ALTER TABLE `Usernames`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Votings`
--
ALTER TABLE `Votings`
  MODIFY `voting_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `DistroDefaultDE`
--
ALTER TABLE `DistroDefaultDE`
  ADD CONSTRAINT `DistroDefaultDE_ibfk_1` FOREIGN KEY (`distro_id`) REFERENCES `Distributions` (`distro_id`),
  ADD CONSTRAINT `DistroDefaultDE_ibfk_2` FOREIGN KEY (`de_id`) REFERENCES `DesktopEnvironment` (`de_id`);

--
-- Constraints for table `DistroPkg`
--
ALTER TABLE `DistroPkg`
  ADD CONSTRAINT `DistroPkg_ibfk_1` FOREIGN KEY (`distro_id`) REFERENCES `Distributions` (`distro_id`),
  ADD CONSTRAINT `DistroPkg_ibfk_2` FOREIGN KEY (`pkg_id`) REFERENCES `Packagemanager` (`pkg_id`);

--
-- Constraints for table `DistroSupportedDE`
--
ALTER TABLE `DistroSupportedDE`
  ADD CONSTRAINT `DistroSupportedDE_ibfk_1` FOREIGN KEY (`distro_id`) REFERENCES `Distributions` (`distro_id`),
  ADD CONSTRAINT `DistroSupportedDE_ibfk_2` FOREIGN KEY (`de_id`) REFERENCES `DesktopEnvironment` (`de_id`);

--
-- Constraints for table `Features`
--
ALTER TABLE `Features`
  ADD CONSTRAINT `Features_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `Questions` (`question_id`),
  ADD CONSTRAINT `Features_ibfk_2` FOREIGN KEY (`distro_id`) REFERENCES `Distributions` (`distro_id`);

--
-- Constraints for table `Passwords`
--
ALTER TABLE `Passwords`
  ADD CONSTRAINT `Passwords_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `Usernames` (`user_id`);

--
-- Constraints for table `UserComments`
--
ALTER TABLE `UserComments`
  ADD CONSTRAINT `UserComments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `Usernames` (`user_id`),
  ADD CONSTRAINT `UserComments_ibfk_2` FOREIGN KEY (`distro_id`) REFERENCES `Distributions` (`distro_id`);

--
-- Constraints for table `Votings`
--
ALTER TABLE `Votings`
  ADD CONSTRAINT `Votings_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `Usernames` (`user_id`),
  ADD CONSTRAINT `Votings_ibfk_2` FOREIGN KEY (`distro_id`) REFERENCES `Distributions` (`distro_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
