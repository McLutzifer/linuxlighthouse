# Commits
This project utilizes [Conventional Commit Messages](https://www.conventionalcommits.org/en/v1.0.0/) that are structured as follows:  

```
<type>[optional scope]: <description>

[optional body]
```

A convenient list of types can be found [here](https://gist.github.com/qoomon/5dfcdf8eec66a051ecd85625518cfd13):

* API relevant changes
    * `feat` Commits, that adds a new feature
    * `fix` Commits, that fixes a bug
* `refactor` Commits, that rewrite/restructure your code, however does not change any behaviour
    * `perf` Commits are special `refactor` commits, that improve performance
* `style` Commits, that do not affect the meaning (white-space, formatting, missing semi-colons, etc)
* `test` Commits, that add missing tests or correcting existing tests
* `docs` Commits, that affect documentation only
* `build` Commits, that affect build components like build tool, ci pipeline, dependencies, project version, ...
* `ops` Commits, that affect operational components like infrastructure, deployment, backup, recovery, ...
* `chore` Miscellaneous commits e.g. modifying `.gitignore`


To automatically close an issue, use the following keywords followed by the issue reference.  
`Closes`, `Fixes`, `Resolves` or `Implements`

# Branches
To automatically close a GitLab issue when the corresponding branch is merged, the branch should be named using the format issue_number-issue_description. This will link the branch to the issue, and when the branch is merged, GitLab will automatically close the associated issue.  
